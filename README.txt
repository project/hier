Hierarchy Module

BRANCH and LEAF entries:
  - Leaf items must be parented to branch items
  - The top-level item is neither (thus we need branches)

CURRENT MAINTAINERS:

  Allie Micka - Advantage Labs, Inc.  http://drupal.org/user/15091

Please use the issue queue to file questions and feature requests on this
module.  If you would like to contract additional development, please contact
us at http://www.advantagelabs.com/contact/

This module provides an efficient and flexible way of storing content in a hierarchy. You can use this hierarchy for site menus, scoped searches, and node references.

INCLUDED MODULES

  * Hierarchy field: use CCK fields to create and maintain node trees for your site
  * Hierarchy menu: Show your hierarchies in the primary and secondary links menu, or from within a correctly-nested block.
  * Hierarchy: A generalized hierarchy API, which stores the hierarchical links and provides views integration

This module is still in devleopment, and your mileage may vary.

TODO
- When an item is deleted, its children are also removed.  However, the delete hook is not effected against subordinate items.
- Multiple hier tables - one for each hierarchy?
- Table indexing
- EFFICIENCY: add hier_field stuff on node_load operations, rather than on view
- Add output formatters for parents/children ( lists, etc. )
- hier_move(), by storing everything in memory pending reinsertion, is grossly inefficient and rather dangerous
- Don't allow the same parent twice for the same hier.
- When a placement is added for an item, should it take its children with it?  As of now, it creates a new, empty hid - but that's inconsistent with 
