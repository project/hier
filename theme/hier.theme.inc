<?php // $Id$

/*
 * Implementation of hook_theme().
 */
function _hier_theme() {
  $path = drupal_get_path('module', 'hier') .'/theme';
  return array(
    'hier_list' => array(
      'arguments' => array('view', 'items' => array(), 'type' => NULL ),
      'file' => 'hier.theme.inc',
      'path' => $path,
    ),
    'hier_array' => array(
      'arguments' => array('tree' => array(), 'values' => array() ),
      'file' => 'hier.theme.inc',
      'path' => $path,
    ),
    'hier_breadcrumbs' => array(
      'file' => 'hier.theme.inc',
      'path' => $path,
    ),
  );
}

/*
 * Theme a hierarchy's breadcrumbs.
 */
function theme_hier_breadcrumbs($hier) {
  if (isset($hier['#item'])) $hier = $hier['#item'];
  if ($hid = $hier['hid']) {
    if ($hier = hier_load($hier)) {
      if ($parents = $hier->item_parents($hid)) {
        foreach ($parents as $h) {
          $items[] = $h->title;
        }
      }
      $h = $hier->load_item($hid);
      $items[] = $h->title;
    }
  }
  if ($items) {
    $output = join(' » ', $items);
  }
  return $output;
}

/*
 * API Function: Return a themed tree view for a given hier tree
 */
function theme_hier_list($view, $items, $type) {
  drupal_add_css(drupal_get_path('module', 'hier') .'/theme/hier.css');
  $out = '';
  foreach ($items as $h) {
    if (!isset($pid)) {
      $pid = $h->pid;
      $out .= '<div class="hier-tree" id="hier-'. $h->pid .'">';
      $lvl = $h->lvl - 1;
    }
    if ($h->lvl <= $lvl) {
      $out .= str_repeat("</div>\n", $lvl - $h->lvl+1);
    }

    $out .= '<div class="hier-item" id="hier-'. $h->hid .'">';
    if ($h->nid) {
      $attributes = array('class' => 'hier-lvl-'. $h->lvl);
      $out .= l($h->title, 'node/'. $h->nid, $attributes);
    }
    else $out .= $h->title;

    $lvl = $h->lvl;
  }
  $out .= str_repeat("</div>\n", $lvl);
  return $out;
}

/*
 * API Function: Return a themed array for a given hier tree
 */
function theme_hier_array($tree, $values = array(), $indicator = '--' ) {
  $group_pid = '';

  foreach ($tree as $h) {

    $title = $h->title;
    $indent = str_repeat($indicator, $h->lvl);

    if (is_array($pids) && count($pids) > 1) {
      if ($h->pid != $group_pid) {
        $group = $title;
        $group_pid = $h->pid;
        $values[$group] = array();
      }
      $values[$group][$h->hid] = $indent .' '. $title;
    }
    else {
      $values[$h->hid] = $indent .' '. $title;
    }
  }
  return $values;
}
