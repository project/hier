<?php // $Id$

class hier {
  var $pid;
  var $handler;
  var $title;
  var $name;
  var $table_name;

  var $read_only = FALSE;

  function __construct($row) {
    $row = (object) $row;
    if (isset($row->data) && is_array($row->data)) {
      foreach ($row->data as $name => $val) {
        $row->$name = $val;
      }
    }
    foreach (array_keys((array) $this) as $var) {
      $func = 'set_'. $var;
      if (method_exists($this, $func)) {
        if ($row && isset($row->$var)) {
          $this->$func($row->$var);
        }
        elseif (!isset($this->$var)) {
          $this->$func();
        }
      }
      else {
        $this->$var = $row->$var;
      }
    }
  }

  function access($op = 'view', $account = NULL) {
    return TRUE;
  }

  function pid() {
    return (int) $this->pid;
  }

  function title() {
    return filter_xss($this->title);
  }

  function name() {
    return filter_xss($this->name);
  }

  function table_name() {
    if (!$this->table_name) {
      $this->set_table_name();
    }
    return check_plain($this->table_name);
  }

  function handler() {
    return check_plain(get_class($this));
  }

  function handler_name() {
    $handlers = hier_handlers();
    return $handlers[$this->handler()]['name'];
  }

  function set_count() {
    if (!isset($this->count)) {
      $this->count = db_result(db_query("SELECT COUNT(1)
        FROM {". $this->table_name() ."}"));
    }
  }

  function count() {
    $this->set_count();
    return (int) $this->count;
  }

  function root_item() {
    $root = (object) array(
      'title' => $this->title(),
      'lvl' => 0,
      'lft' => 1,
      'rgt' => 2,
      'branch' => 1,
    );
    return $root;
  }

  function save() {
    $update = $this->pid ? 'pid' : NULL;

    $this->data = array();
    foreach (array_keys((array) $this) as $var) {
      if (!in_array($var, array('pid', 'handler', 'name', 'title', 'table_name'))) {
        if (method_exists($this, 'set_'. $var)) {
          $this->data[$var] = $this->$var;
        }
      }
    }
    drupal_write_record('hier_list', $this, $update);

    // Update the root to reflect this hierarchy's name.
    if ($update && !$this->read_only && $this->table_name()) {
      db_query("UPDATE {". $this->table_name() ."} SET title = '%s'
        WHERE lvl = 0 LIMIT 1", $this->title());
    }
    else {
      $ret = array();
      if ($schema = $this->schema()) {
        foreach ($schema as $name => $table) {
          db_create_table($ret, $name, $table);
        }

        // Reset the schema cache.
        drupal_get_schema(NULL, TRUE);

        // Add the root item.
        $root = $this->root_item();
        drupal_write_record($this->table_name(), $root);
      }
    }
    return TRUE;
  }

  function delete() {
    db_query("DELETE FROM {hier_list} WHERE pid = %d", $this->pid);
    foreach($this->schema() as $name => $table) {
      $ret = array();
      db_drop_table($ret, $name);
    }
  }

  function set_handler() {
    $this->handler = get_class($this);
  }

  function set_title($value = NULL) {
    if (!$value) {
    }
    $this->title = $value;
  }

  function set_name($value = NULL) {
    if (!$value) {
      $value = $this->title();
    }
    $value = preg_replace(array('/\s+/', '/[^\w-]/', '/(^-|-$)/'), array('_', ''), drupal_strtolower($value));
    $this->name = $value;
  }

  function set_table_name($value = NULL) {
    if (!$value && $this->name()) {
      $value = 'hier_'. $this->name();
    }
    $value = preg_replace(array('/\s+/', '/[^\w-]/', '/(^-|-$)/'), array('_', ''), drupal_strtolower($value));
    $this->table_name = $value;
  }

  function settings_form() {
    return array(
      '#input' => TRUE,
      '#tree' => TRUE,
      '#process' => array('hier_settings_process'),
      'pid' => array('#type' => 'value', '#value' => $this->pid()),
      'handler' => array('#type' => 'value', '#value' => $this->handler()),
      'name' => array('#type' => 'value', '#value' => $this->name()),
    );
  }

  function schema() {
    if ($table = $this->table_name()) {
      return array(
        $table => array(
          'fields' => array(
            'hid' => array(
              'type' => 'serial',
              'unsigned' => TRUE,
              'views' => array(
                'filter' => array(
                  'handler' => 'hier_views_handler_filter',
                ),
                'argument' => array(
                  'handler' => 'hier_views_handler_argument',
                ),
                'sort' => array(
                  'handler' => 'hier_views_handler_sort',
                ),
              ),
            ),
            'title' => array(
              'type' => 'varchar',
              'length' => 100,
              'views' => array(
                'filter' => array(
                  'handler' => 'views_handler_filter',
                ),
                'argument' => array(
                  'handler' => 'views_handler_argument_string',
                ),
                'sort' => array(
                  'handler' => 'views_handler_sort',
                ),
              ),
            ),
            'lft' => array(
              'type' => 'int',
              'unsigned' => TRUE,
              'not null' => TRUE,
            ),
            'rgt' => array(
              'type' => 'int',
              'unsigned' => TRUE,
              'not null' => TRUE,
            ),
            'lvl' => array(
              'type' => 'int',
              'unsigned' => TRUE,
              'not null' => TRUE,
            ),
            'branch' => array(
              'type' => 'int',
              'size' => 'tiny',
              'not null' => TRUE,
              'default' => 0,
            ),
          ),
          'indexes' => array(
            $table .'_lft' => array('lft'),
          ),
          'primary key' => array('hid'),
        ),
      );
    }
  }

  function views_data() {
  }

  /*
   * Return a full tree for one or more hierarchies
   */
  function tree($filter = array(), $depth = NULL, $dir = '+') {
    $tree = array();
    $table = $this->table_name();

    $table_alias = check_plain($this->name());

    $where = $having = $args = array();
    foreach($filter as $key => $val) {
      $this->_tree_filter($table_alias .'.'. $key, $val, $where);
    }
    if ($depth_sql = $this->tree_depth_filter_query($table_alias, $depth)) {
      $having[] = $depth_sql;
    }

    $sql = "SELECT $table_alias.* FROM ";
    $sql .= $this->tree_table_query($table_alias, $dir);
    $sql .= ($where) ? 'WHERE '. join(' AND ', $where) : ' ';
    $sql .= " GROUP BY $table_alias.hid ";
    $sql .= ($having) ? 'HAVING '. join(' AND ', $having) : ' ';
    $sql .= $this->tree_sort_query($table_alias);
    
    drupal_set_message(print_r($sql, 1));

    $res = db_query($sql, $args);

    while ($row = db_fetch_object($res)) {
      $tree[$row->hid] = $row;
    }
    return $tree;
  }

  function tree_table_query($table_alias = NULL, $dir = '+') {
    if (is_null($table_alias)) $table_alias = $this->table_name();

    $table = check_plain($this->table_name());
    $h1 = check_plain($table_alias);
    $h2 = $h1 .'2';

    $p = ($dir == '+') ? $h2 : $h1;
    $c = ($dir == '+') ? $h1 : $h2;

    return "{$table} $h1 
      LEFT JOIN {$table} $h2 ON $c.lft BETWEEN $p.lft AND $p.rgt ";
  }

  function tree_depth_filter_query($table_alias = NULL, $depth = NULL) {
    if (is_null($table_alias)) $table_alias = $this->table_name();
    if (!is_null($depth)) {
      $depth = (int) $depth + 2;
      return '(COUNT('. $table_alias .'2.hid) < '. $depth .')';
    }
    //return '(ABS('. $table_alias .'.lvl) - ABS('. $table_alias .'2.lvl)) < %d';
  }

  function tree_sort_query($table_alias = NULL, $dir = 'ASC') {
    if (is_null($table_alias)) $table_alias = $this->table_name();
    $table_alias = check_plain($table_alias);
    $dir = check_plain($dir);

    return " ORDER BY $table_alias.lft $dir";
  }

  function tree_groupby_column() {
    return 'hid';
  }

  function tree_sort_column() {
    return 'lft';
  }

  /*
   * Helper function to process filters for hier_tree()
   */
  function _tree_filter($name, $val, &$where) {
    if (is_null($val)) return;
    if (is_array($val) && !$val) return;

    if (is_numeric($val)) {
      $where[] = "$name = ". (int) $val;
    }
    elseif (is_array($val)) {
      foreach ($val as $i => $n) {
        if (is_numeric($n)) $val[$i] = (int) $n;
        else unset($val[$i]);
      }
      $where[] = "$name IN (". join(', ', $val) .")";
    }
  }

  /*
   * Add an item to the hierarchy
   * $item is most likely either a node or a $h object.
   */
  function insert_item($item, $parent_hid) {
    if ($this->read_only) return FALSE;

    $item = $this->load_item($item);
    unset($this->count);

    // Don't permit re-adding an existing relationship.
    if (isset($item->hid)) {
      if (key($this->item_parents($item->hid, 1)) == $parent_hid) {
        return $item->hid;
      }
    }

    // TODO this is causing issues...
    //db_lock_table($this->table_name());

    // Fetch parent item
    if (!$p = db_fetch_object(db_query("SELECT *
      FROM {". $this->table_name() ."} WHERE hid = %d", $parent_hid))) {

      //db_unlock_tables();
      return FALSE;
    }

    db_query("UPDATE {". $this->table_name() ."} SET
      lft = CASE WHEN lft >  %d THEN lft + 2 ELSE lft END,
      rgt = CASE WHEN rgt >= %d THEN rgt + 2 ELSE rgt END
      WHERE rgt >= %d ", $p->rgt, $p->rgt, $p->rgt);

    $item->lft = $p->rgt;
    $item->rgt = $p->rgt+1;
    $item->lvl = $p->lvl+1;

    $item->save();
    //db_unlock_tables();

    // Recursively add any children this item may have
    if (isset($item->children)) {
      foreach ($item->children as $child) {
        $this->insert_item($child, $hid);
      }
    }
    module_invoke_all('hier', 'insert', $this, $this->load_item($item->hid));

    // Unset any cached entries
    $cid = 'hier_%'. $this->pid .':%:'. $nid .':';
    cache_clear_all($cid, 'cache', TRUE);

    return $item->hid;
  }

  /*
   * Delete an item and its children from the hierarchy
   * For now, we're not doing the re-parenting thing.
   */
  function delete_item($hid) {
    if ($this->read_only) return FALSE;

    unset($this->count);

    // Don't allow deletion of the root item.
    if ($hid == 1) {
      return FALSE;
    }

    $h   = $this->load_item($hid);
    $rgt = $h->rgt;
    $w   = $h->rgt - $h->lft + 1;

    db_query("DELETE FROM {". $this->table_name() ."} 
      WHERE lft BETWEEN %d AND %d AND lvl > 0", $h->lft, $h->rgt);

    db_query("UPDATE {". $this->table_name() ."} SET
      lft = CASE WHEN lft >  %d THEN lft - %d ELSE lft END,
      rgt = CASE WHEN rgt >= %d THEN rgt - %d ELSE rgt END
      WHERE rgt >= %d", $rgt, $w, $rgt, $w, $rgt);

    module_invoke_all('hier', 'delete', $this, $h);

    // Unset any cached entries
    $cid = 'hier_%'. $this->pid .':%:'. $h->nid .':';
    cache_clear_all($cid, 'cache', TRUE);
  }

  /*
   * Move to a new parent in the hierarchy
   */
  function move_item($hid, $parent_hid) {
    if ($this->read_only) return FALSE;

    // TODO lock tables here.

    // Safeguard:  Don't permit an item to be its own parent.
    // TODO I'm not sure if this is doing what it's supposed to do.
    $parents = $this->item_parents($hid);
    if (isset($parents[$hid])) {
      drupal_set_message('Unable to add an item as its own child', 'error');
      return FALSE;
    }

    // Fetch existing hier item.
    if (!$h =  $this->load_item($hid)) {
      return FALSE;
    }

    // Fetch new parent item.
    if (!$p =  $this->load_item($parent_hid)) {
      return FALSE;
    }

    // Original parent item.
    $o = (end($parents));

    // Sanitizing to avoid a brain-breaking query substitution.
    $hl = (int) $h->lft;
    $hr = (int) $h->rgt;

    $ol = (int) $o->lft;
    $or = (int) $o->rgt;

    $pl = (int) $p->lft;
    $pr = (int) $p->rgt;
    $width = $h->rgt - $h->lft + 1;

    // Move the item into the new slot and close the original gap.
    // TODO THIS ISN'T WORKING
    db_query("UPDATE {". $this->table_name() ."} SET
      lft =
        CASE WHEN (lft BETWEEN $ol AND $or) THEN (lft - $ol)
        WHEN (lft >= $hl AND rgt <= $hr) THEN ($pl + $hr - lft)
        ELSE lft END,
      rgt =
        CASE WHEN (lft =  ($ol) AND rgt = ($or)) THEN ($hl + 1)
        WHEN (lft >= ($hl) AND rgt <= ($hr)) THEN ($pl + ($hr - rgt))
        WHEN (lft >= ($pl) AND rgt <= ($pr)) THEN ($pl + $width)
        ELSE rgt END
     ");

    return $hid;
  }

  function save_item($hid) {
    if ($this->read_only) return FALSE;
    if ($h =  $this->load_item($hid)) {
      $h->save();
    }
  }

  function swap_items($hid_1, $hid_2) {
    if ($this->read_only) return FALSE;

    db_lock_table($this->table_name());

    // Fetch left item.
    if (!$h1 = $this->load_item($hid_1)) {
      db_unlock_tables();
      return FALSE;
    }

    // Fetch right item.
    if (!$h2 = $this->load_item($hid_2)) {
      db_unlock_tables();
      return FALSE;
    }

    // They must be at the same level to be swappable.
    if ($h1->lvl != $h2->lvl) {
      db_unlock_tables();
      return FALSE;
    }

    // This function allows siblings in any order, but the forthcoming SQL
    // needs the items to be in left-to-right order.  Set parameters accordingly,
    // Sanitizing to avoid a brain-breaking query substitution.
    $l1 = (int) min($h1->lft, $h2->lft);
    $r1 = (int) min($h1->rgt, $h2->rgt);
    $l2 = (int) max($h1->lft, $h2->lft);
    $r2 = (int) max($h1->rgt, $h2->rgt);

    db_query("UPDATE {". $this->table_name() ."} SET
      lft =
        CASE WHEN lft BETWEEN $l1 AND $r1 THEN $r2 + lft - $r1
        WHEN lft BETWEEN $l2 AND $r2 THEN $l1 + lft - $l2
        ELSE $l1 + $r2 + lft - $r1 - $l2 END,
      rgt =
        CASE WHEN rgt BETWEEN $l1 AND $r1 THEN $r2 + rgt - $r1
        WHEN rgt BETWEEN $l2 AND $r2 THEN $l1 + rgt - $l2
        ELSE $l1 + $r2 + rgt - $r1 - $l2 END
      WHERE lft BETWEEN $l1 AND $r2
        AND $l1 < $r1
        AND $r1 < $l2
        AND $l2 < $r2");

    db_unlock_tables();
  }

  function load_item($hid) {
    $item = New hier_item($hid, $this);
    return $item;
  }

  function item_parents($hid, $depth = NULL, $filter = array()) {
    $filter = array_merge($filter, array('hid' => $hid));
    if (!is_null($depth)) $depth++;
    $tree = $this->tree($filter, $depth, '-');
    unset($tree[$hid]);
    return $tree;
  }

  function item_children($hid, $depth = NULL, $filter = array()) {
    $filter = array_merge($filter, array('hid' => $hid));
    if (!is_null($depth)) $depth++;
    return $this->tree($filter, $depth, '+');
    unset($tree[$hid]);
    return $tree;
  }
}

class hier_item {
  var $hier;
  var $hid;
  var $lft;
  var $rgt;
  var $lvl;
  var $branch = 0;

  /*
   * Load a row from the hier table based on the unique hid key
   */
  function __construct($hid, $hier) {
    $this->hier = $hier;
    if (is_numeric($hid)) {
      $row = db_fetch_object(db_query("SELECT * FROM {". $hier->table_name() ."
        WHERE hid = %d", $hid)); 
    }
    else {
      $row = $hid;
    }
    if (is_object($row)) {
      foreach ((array)$row as $name => $val) {
        $this->$name = $val;
      }
    }
  }

  function parents($depth = NULL, $filter = array()) {
    return $this->hier->item_parents($this->hid, $depth, $filter);
  }

  function children($depth = NULL, $filter = array()) {
    return $this->hier->item_children($this->hid, $depth, $filter);
  }

  function save() {
    $update = isset($this->hid) ? 'hid' : NULL;
    drupal_write_record($this->hier->table_name(), $this, $update);
  }
}
