<?php //$Id$

class hier_field extends hier {
  var $name;
  var $field;
  var $content_types;
  var $branch_types;

  function field() {
    if (!isset($this->field) || !is_array($this->field)) {
      $this->set_field($this->field);
    }
  }

  function set_field($field = NULL) {
    if (!isset($field)) {
      $field = $this->name();
    }
    if ($field) {
      if (!is_array($field)) {
        $field = content_fields($field);
      }
      if (isset($field['hier'])) {
        foreach ($field['hier'] as $name => $val) {
          if ($name != 'field' && method_exists($this, $func = 'set_'. $name)) {
            $this->$func($val);
          }
        }
        unset($field['hier']);
      }

      $this->field = $field;
      $this->set_title($field['widget']['label']);
      $this->set_name($field['field_name']);
      $this->set_table_name();
    }
  }

  function content_types() {
    $this->set_content_types();
    return $this->content_types;
  }

  function set_content_types() {
    if (!isset($this->content_types)) {
      $this->content_types = array();
      $info = _content_type_info();
      foreach ($info['content types'] as $type => $content_type) {
        if (isset($content_type['fields'][$this->name()])) {
          $this->content_types[$type] = $content_type['name'];
        }
      }
    }
  }

  function set_branch_types($value = NULL) {
    if (!is_null($value)) {
      $this->branch_types = $value;
    }
    elseif (!isset($this->branch_types)) {
      $this->branch_types = $this->content_types();
    }
  }

  function branch_types() {
    $this->set_branch_types();
    return array_keys(array_filter($this->branch_types));
  }

  function schema() {
    // Start with the default schema for a hierarchy.
    $schema = parent::schema();
    $table  = $this->table_name();

    // Add a nid column to track nodes placed into this hierarchy.
    $schema[$table]['fields']['nid'] = array(
      'type' => 'int',
      'unsigned' => 'TRUE',
      'not null' => 'TRUE',
      'default' => 0,
      'views' => array(
        'relationship' => array(
           'base' => 'node',
           'field' => 'nid',
        ),
      ),
    );

    // Add an index for it too.
    $schema[$table]['indexes'][$table .'_nid'] = array('nid');

    return $schema;
  }

  function views_data() {
    // Called from hook_views_data, return my relationship to the node table.
    $data[$this->table_name()]['table']['join'] = array(
      'node' => array(
        'left_field' => 'nid',
        'field' => 'nid',
      ),
    );

    return $data;
  }

  function settings_form() {
    $form = parent::settings_form();

    // Hide the "name" form element and use field name instead.
    $form['name'] = array(
      '#type' => 'value',
      '#value' => $this->field['field_name'],
    );

    // Hide the "title" form element and use the widget label instead.
    $form['title'] = array(
      '#type' => 'value',
      '#value' => $this->field['widget']['label'],
    );

    // Any content type with this field can appear in the hierarchy, but only
    // some may appear as parent items.
    $form['branch_types'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Branch content types'),
      '#description' => t('Select the content types that should be available as parents in the hierarchy.'),
      '#required' => TRUE,
      '#options' => $this->content_types(),
      '#default_value' => $this->branch_types(),
    );

    return $form;
  }

  function save() {
    // Perform updates if required.
    $old = hier_load($this->pid);
    if ($old->branch_types() != $this->branch_types()) {
      $types = "'". join("', '", $this->branch_types()) ."'";
      db_query("UPDATE {". $this->table_name() ."} h 
        INNER JOIN {node} n USING ( nid )
        SET branch = CASE WHEN n.type IN ( $types ) THEN 1 ELSE 0 END");
    }

    parent::save();
  }

  function insert_item($item, $parent_hid) {
    // Set the node's nid and "branchiness" (based on the its content type).
    $item->branch = (int) in_array($item->type, $this->branch_types());

    // Proceed as usual.
    return parent::insert_item($item, $parent_hid);
  }
}
