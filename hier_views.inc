<?php //$Id$

function _hier_views() {
  views_load_cache();

  $views = array();
  $res = db_query("SELECT DISTINCT vid FROM {view_filter}
    WHERE field LIKE 'hier.%'
    OR field LIKE 'hier_parents_%' OR field LIKE 'hier_children_%'");

  while ($row = db_fetch_object($res)) {
    $views[$row->vid] = views_get_view($row->vid);
  }

  $tables = array('hier', 'hier_parents_', 'hier_children_');
  foreach (_views_get_default_views() as $view) {
    if (isset($view->requires)) {
      foreach ($view->requires as $req) {
        if (in_array(preg_replace('/\d*$/', '', $req), $tables)) {
          $views[$view->name] = $view;
          continue;
        }
      }
    }
  }

  cache_set('hier_views', 'cache_views', serialize($views));

  return $views;
}

function _hier_views_tables() {
  /*
    filters: "is a hierarchy", "depth", "in current hierarchy", (add "current" to other filter handlers)
  */
  $tables = array();
  $tables['hier'] = array(
    'name'     => 'hier',
    'join'     => array(
      'left'    => array(
        'table'    => 'node',
        'field'    => 'nid',
      ),
      'right'   => array(
        'field'    => 'nid',
      ),
    ),

    'fields'    => array(
      'pid'      => array(
        'name'     => t('Hierarchy: Hierarchy name'),
        'sortable' => true,
        'help'     => t('The hierarchies the item appears in.'),
        // todo name handler (show name rather than pid, etc)
      ),
      'lvl'      => array(
        'name'     => t('Hierarchy: Hierarchy level'),
        'sortable' => true,
        'help'     => t('Level in tree.'),
      ),
      'hid'      => array(
        'name'     => t('Hierarchy: Hierarchy id'),
        'sortable' => true,
      ),
    ),

    'filters'   => array(
      'pid'      => array(
        'name'     => t('Hierarchy: Hierarchy name'),
        'help'     => t('The name of the hierarchy'),
        'operator' => 'views_handler_operator_andor',
        'list'     => 'hier_list',
        'value-type' => 'array',
        // todo handler to show name vs item
      ),
      'lvl'    => array(
        'name'     => t('Hierarchy: Level in tree'),
        'help'     => t('How many levels deep the item appears in the hierarchy.  Best used with a Hierarchy name filter'),
        'operator' => 'views_handler_operator_gtlt',
        'option'   => array('#type' => 'select', '#options' => range(0,20)),
        // todo this doesn't appear correctly on exposed filters page.
      ),
    ),

    'sorts'     => array(
      'hier'      => array(
        'name'      => t('Hierarchy: Position in hierarchy'),
        'handler'   => 'hier_views_sort_hier',
      ),
    ),
  );

  foreach (hier_list('option') as $pid => $title) {
    $tables['hier_parents_'. $pid] = array(
      'name'   => 'hier',
      'join'   => array(
        'type' => 'left',
        'left' => array( 'table' => 'hier', 'field' => 'pid' ),
        'right' => array( 'field' => 'pid' ),
        'extra' => array(
          'pid' => $pid,
          'lft <= hier.lft' => null,
          'rgt >= hier.rgt' => null,
         ),
      ),

      'filters' => array(
        'parent' => array(
          'name'     => t('Hierarchy: Parent for @title', array('@title' => $title)),
          'help'  => t('Select only the items BELOW the chosen node.'),
          'handler'  => 'hier_views_filter_tree',
          'operator' => array('=' => t('Is'), '?' => t('Includes')),
          // TODO: this is a static form, created at cache time. Views says we can just have 'function_name' as the #options, but this doesn't work right now
          'value'     => array (
            '#type' => 'select',
            '#options' => '_hier_views_tree_select',
            ),
          'value-type' => 'array',
          'pid' => $pid,
        ),
      ),
      // Fields: "Up" link, "Back" link, "Next" link for tree navigation.
    );

    $tables['hier_children_'. $pid] = array(
      'name'   => 'hier',

      'join'   => array(
        'type' => 'left',
        'left' => array( 'table' => 'hier', 'field' => 'pid' ),
        'right' => array( 'field' => 'pid' ),
        'extra' => array(
          'pid' => $pid,
          'lft >= hier.lft' => null,
          'rgt <= hier.rgt' => null,
         ),
      ),

      'fields' => array(
        'count' => array(
          'name'     => t('Hierarchy: Number of children for @title', array('@title' => $title)),
          'notafield'=> true,
          'query_handler' => 'hier_views_handler_child_count',
        ),
      ),

      'filters' => array(
        'child' => array(
          'name'     => t('Hierarchy: Child for @title', array('@title' => $title)),
          'help'  => t('Select only the items ABOVE the chosen node.'),
          'handler'  => 'hier_views_filter_tree',
          'operator' => array('=' => t('Is'), '?' => t('Includes')),
          // TODO: this is a static form, created at cache time. Views says we can just have 'function_name' as the #options, but this doesn't work right now
          'value'     => array (
            '#type' => 'select',
            '#options' => '_hier_views_tree_select',
            ),
          'value-type' => 'array',
          'pid' => $pid,
        ),
      ),
    );
  }
  return $tables;
}

/*
 * Implementation of hook_views_arguments()
 */
function _hier_views_arguments() {
  $arguments = array();

  $depth_option = range(0,20);
  $depth_option[0] = t('Max distance');

  $arguments['hier_pid'] = array(
    'name'     => t('Hierarchy: Name of hierarchy'),
    'handler'  => 'hier_views_arg_pid',
  );

  foreach (hier_list('option') as $pid => $name) {
    $arguments['hier_parents_'. $pid] = array(
      'name'     => t('Hierarchy: Parents for @name', array('@name' => $name)),
      'handler'  => 'hier_views_arg_tree',
      'option'   => array( '#type' => 'select', '#options' => $depth_option ),
    );
    $arguments['hier_children_'. $pid] = array(
      'name'     => t('Hierarchy: Children for @name', array('@name' => $name)),
      'handler'  => 'hier_views_arg_tree',
      'option'   => array( '#type' => 'select', '#options' => $depth_option ),
    );
  }
  return $arguments;
}

/*
 * Implementation of hook_views_default_views()
 */
function _hier_views_default_views() {
  $views = array();
  // navigation block (for each pid): 1 < level < 4, and shares all my parents

  // sitemap
  return $views;
}

/*
 * Implementation of hook_views_style_plugins()
 */
function _hier_views_style_plugins() {
  return array(
    'hier'    => array(
      'name'    => t('Hierarchy'),
      'theme'   => 'hier_list',
      'validate' => 'hier_list_plugin_validate',
    ),
  );
}

function hier_list_plugin_validate($type, $view, $form) {
  if (!$view['sort'] || ($view['sort'][0]['id'] != 'hier.hier')) {
    if (isset($view['sort']['add']['id']) && $view['sort']['add']['id'] == 'hier.hier') return;
    form_error($form['sort'], t('The hierarchy view type requires you to sort by Hierarchy'));
  }
}

function hier_views_filter_lvl($op, $filterdata = null, $filterinfo = null, &$query = null) {
  switch ($op) {
    case 'operator':
      return array('=' => t('Is'), '?' => t('Includes'));

    case 'option':
      $lvls = range(0, 20);
      $lvls[0] = t('Root');
      return $lvls;
  }
}

function hier_views_handler_child_count($field, $info, &$query) {
  $query->add_table($table = $field['tablename']);
  $query->add_field("COUNT($table.hid) -1", NULL, $field['queryname']);
  $query->add_groupby("{hier}.hid");
}

function hier_views_filter_tree($op, $filterdata = null, $filterinfo = null, &$query = null) {
  $pid  = preg_replace('/[^\d]* /', '', $filterdata['table']);

  switch ($op) {
    case 'operator':
      return array('=' => t('Is'), '?' => t('Includes'));

    case 'value':
      return array (
        '#type' => 'select',
        '#options' => '_hier_views_tree_select',
      );

    case 'handler':
      $table = isset($filterinfo['tablename']) ? $filterinfo['tablename'] : $filterinfo['table'];
      $query->ensure_table($table, false, 1);
      $value = $filterdata['value'];
      if (is_array($value)) {
        $value = implode(', ', $value);
      }
      $query->add_where("$table.hid IN ( $value )");

      // find only immediate parents/children
      if ($filterdata['operator'] == '=') {
        $query->add_where("ABS($table.lvl - {hier}.lvl) = 1");
      }
      return;
  }
}

function _hier_views_tree_select($op, $filterinfo) {
  $pid = $filterinfo['pid'];
  $values = array(
    '***HIER_'. $pid .'_HID***'          => t('Current node'),
    '***HIER_'. $pid .'_POS***'       => t('Current position in hierarchy'),
  );
  $tree   = hier_tree($pid, null, null, 3);
  $values = theme('hier_tree', $tree[$pid], $values);
  return $values;
}

/*
 * Argument handling for hierarchy name
 */
function hier_views_arg_pid($op, &$query, $a1, $a2=null) {
  switch ($op) {
    case 'filter':
      if (!is_numeric($pid = $a2)) {
        if (!$pid = db_result(db_query("SELECT pid FROM {hier_list} WHERE title = '%s'", $a2))) {
          return false;
        }
      }
      $query->where[] = "%s.pid = %d";
      $query->where_args[] = 'hier';
      $query->where_args[] = $pid;
      return;
  }
}

/*
 * Argument handling for parent/child arguments
 */
function hier_views_arg_tree($op, &$query, $a1, $a2=null) {
  switch ($op) {
    case 'filter':
      // Whether we're dealing with a "parents" or "children" query.
      $type = preg_replace('/^hier_|_\d+/', '', $a1['type']);
      $pid  = preg_replace('/[^\d]*/', '', $a1['type']);
      $query->ensure_table('hier');
/*
      $query->add_table('hier_'. $type .'_'. $pid, false, 1);

      // Allow the use of nid or name from the hierarchy table
      if (is_numeric($a2)) {
        $query->where[] = "%s.nid = %d";
        $query->where_args[] = 'hier_'. $type .'_'. $pid; //todo aliased?
        $query->where_args[] = $a2;
      }
      else {
        $query->where[] = "%s.title = '%s'";
        $query->where_args[] = 'hier_'. $type .'_'. $pid; //todo aliased?
        $query->where_args[] = $a2;
      }
*/
      $table = 'hier_'. $type .'_'. $pid;
      $table = 'hier';

      if ($type == 'parent') {
        $query->add_where('%s.nid IN ( SELECT hier.nid FROM %s hier INNER JOIN %s hier2 ON hier.pid = %d AND hier2.pid = %d AND hier2.lft >= hier.lft AND hier2.rgt <= hier.rgt AND hier2.nid IN ( %s )) ', $table, $table, $table, $pid, $pid, $a2);
      } else {
        $query->add_where('%s.nid IN ( SELECT hier.nid FROM %s hier INNER JOIN %s hier2 ON hier.pid = %d AND hier2.pid = %d AND hier2.lft <= hier.lft AND hier2.rgt >= hier.rgt AND hier2.nid IN ( %s )) ', $table, $table, $table, $pid, $pid, $a2);
      }

      // Limit distance in tree
      if ($a1['options']) {
        $query->add_field('lvl', 'hier');
        $query->where[] = "ABS(%s.lvl - %s.lvl) <= %d";
        $query->where_args[] = 'hier_'. $type .'_'. $pid; //todo aliased?
        $query->where_args[] = 'hier';
        $query->where_args[] = $a1['options'];
      }
      return;

    case 'title':
      $pid  = preg_replace('/^\d*/', '', $a1);
      $name = $query;
      return;
  }
}

/*
 * Sort handling for hierarchies
 */
function hier_views_sort_hier($op, &$query, $sortinfo, $sort) {
  $query->ensure_table('hier', false, 1);

  // todo these are needed by hier tree-related functions, but they
  // probably don't belong here.
  $query->add_field('title', 'node');
  $query->add_field('hid', 'hier');
  $query->add_field('pid', 'hier');
  $query->add_field('lvl', 'hier');

  $query->add_orderby('hier', 'pid', $sort['sortorder']);
  $query->add_orderby('hier', 'lft', $sort['sortorder']);
  $query->add_orderby('hier', 'lvl', $sort['sortorder']);
}
