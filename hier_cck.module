<?php

/**
 * Implementation of hook_menu().
 */
function hier_cck_menu($may_cache) {
  if ($may_cache) {
    $items[] = array(
      'path' => 'hier_cck_widget/js',
      'callback' => 'hier_cck_widget_js',
      'title' => 'hier_cck_widget_js',
      'type' => MENU_CALLBACK,
      'access' => true,
    );
    return $items;
  }
}

/**
 * Implementation of hook_field_info().
 */
function hier_cck_field_info() {
  return array(
    'hier' => array('label' => 'Node Hierarchy'),
  );
}

/**
 * Implementation of hook_field_settings().
 */
function hier_cck_field_settings($op, &$field) {

  switch ($op) {
    case 'form':
      $form = array();
      $form['pid'] = array ('#type' => 'value', '#value' => $field['pid']);
      $form = array_merge($form, hier_cck_view_form($field));

      return $form;

    case 'validate':
      // Update the hierarchy's title based on the field label
      hier_update($field['pid'], $field['label']);
      return;

    case 'save':
      return array('pid', 'view', 'types');

    case 'database columns':
      return array(
        'hid'  => array('type' => 'int', 'unsigned' => true, 'not null' => true),
      );

    case 'filters':
      if (!$pid = $field['pid']) return;

      // Cop filters from hierarchy tables
      $filters = array();
      $tables  = hier_views_tables();
      $options = range(0, 10);
      $options[0] = t("Limit depth");
      foreach (array('hier_parents_'. $pid, 'hier_children_'. $pid) as $table) {
        foreach($tables[$table]['filters'] as $k => $filter) {
          $filter['name'] = str_replace('Hierarchy: ', '', $filter['name']);
          $filter['list'] = 'hier_cck_filter_options';
          $filter['tablename'] = $table;
          $filter['option'] = array('#type' => 'select', '#options' => $options);

          $filters[$k] = $filter;
        }
      }
      return $filters;
  }
}

/**
 * Helper function to produce a views configuration form
 * For now, we take the desiered node type(s) and bake it all into a view
 * Later, we may provide more of the views UI and/or link to existing views.
 */
function hier_cck_view_form($field) {
  if (!module_exists('views')) return array();

  $form = array();
  $form['view'] = array('#type' => 'value', '#value' => $field['view']);
  $types = node_get_types('names');
  $form['types'] = array(
    '#type'           => 'checkboxes',
    '#title'           => t('Content types that can be referenced'),
    '#multiple'       => TRUE,
    '#default_value'  => isset($field['types']) ? $field['types'] : $types,
    '#options' =>     $types,
  );
  return $form;
}

function hier_cck_view_save(&$field) {
  if (!module_exists('views')) return;

  $title = $field['widget']['label'];
  $name  = $field['field_name'];
  $types = $field['types'] ? array_filter($field['types']) : array();
  $types = array_keys($types);
  $url   = 'hier/'. $name;

  if ($field['view'] && $view = views_load_view($field['view'])) {
    $view->description = t('!name items', array('!name' => $title));

    // Update the view with the new filter data, if necessary.
    foreach ($view->filter as $i => $filter) {
      if ($filter['field'] == 'node.type' && $filter['value'] != $types) {
        unset($view->filter[$i]);
        views_view_add_filter($view, 'node', 'node.type', 'OR', $types, '');
        _views_save_view($view);
      }
    }
    return false;
  }
  else {
    $view = new stdClass();
    $view = _views_check_arrays($view);
    $view->name = $name;
    $view->description = t('!name items', array('!name' => $label));

    views_view_add_filter($view, 'hier', 'hier.pid', '=', $field['pid'], '');
    views_view_add_filter($view, 'node', 'node.status', '=', '1', '');
    views_view_add_page($view, $label, $url, 'hier', false, 0, '', 1);
    views_view_add_sort($view, 'hier', 'hier.hier', 'ASC', '');

    if ($types) {
      views_view_add_filter($view, 'node', 'node.type', 'OR', $types, '');
    }
    $field['view'] = _views_save_view($view);
    views_invalidate_cache();
    return true;
  }
}

function hier_cck_session($field, $name = null, $value = null) {
  $key = $field['field_name'] .'-'. $field['content_type'];
  if ($name) {
    if ($value) $_SESSION[$key][$name] = $value;
    return $_SESSION[$key][$name];
  }
  else {
    unset($_SESSION[$key]);
  }
}

/**
 * Implementation of hook_field().
 */
function hier_cck_field($op, &$node, $field, &$items, $teaser, $page) {
  switch ($op) {

    case 'validate':
      if ($field['required']) {

        // This item is the root node. It's hidden so there's no need for data.
        $root = current(hier_cck_tree($field));
        if ($root->nid == $node->nid) return;

        $validate = $items;
        foreach($validate as $k => $i) {
          if ($i['parent'] == 0) unset($validate[$k]);
        }
        if (!$validate) {
          form_set_error($field['field_name'], t('!name field is required.', array('!name' => $field['widget']['label'])));
        }
      }
      return;

    case 'insert':
      hier_cck_session($field);
      foreach($items as $i => $item) {
        if (isset($item['hid']) && $item['hid']) continue;
        $items[$i]['hid'] = hier_insert($node, $item['parent'], $field['pid']);
      }
      return;

    case 'update':
      hier_cck_session($field);

      $parents = hier_parents($field['pid'], null, $node->nid, 1);
      $parents = $parents ? array_keys(current($parents)) : array();

      foreach ($items as $i => $item) {
        if (!isset($item['parent'])) {
          continue; // We must have a parent id for any of these operations to matter.
        }

        $hid = $item['hid'];
        $parent_hid = $item['parent'];

        // Add newly-selected item.
        if (!$hid && $parent_hid) {
          $items[$i]['hid'] = hier_insert($node, $parent_hid, $field['pid']);
          continue;
        }

        // Item has been removed from the tree.
        if ($hid && $parent_hid == 0) {
          hier_delete($hid);
          unset($items[$i]);
          continue;
        }

        // Item is being moved.
        if ($hid && !in_array($parent_hid, $parents)) {
          hier_move($hid, $parent_hid);
          continue;
        }
      }
      return;

    case 'delete':
      hier_cck_session($field);
      foreach($items as $i => $item) {
        hier_delete($item['hid']);
      }
      return;
  }
}

/**
 * Implementation of hook_field_formatter_info().
 */
function hier_cck_field_formatter_info() {
  return array(
    'default' => array(
      'label' => 'Default',
      'field types' => array('hier'),
    ),
    'plain' => array(
      'label' => 'Plain text (no link)',
      'field types' => array('hier'),
    ),
  );
}

/**
 * Implementation of hook_field_formatter().
 */
function hier_cck_field_formatter($field, $item, $formatter, $node) {
  $item = (array) $item; // safety cast to make sure it is not an object
  $text = '';

  if (!empty($item['hid'])) {
    $parents = hier_parents($field['pid'], $item['hid']);
    $parents = $parents[$field['pid']];

    // subtract 1 for 0-orderness, subtract 1 because we don't want this item in the list
    $stop = count($parents) - 2;
    $output = array();
    for($i = 0; $i < $stop; $i++) {
      $parent = next($parents);
      switch ($formatter) {
        case 'plain':
          $output[] = strip_tags($parent['title']);
          break;

        default:
          $output[] = l(strip_tags($parent['title']), 'node/' . $parent['nid']);
          break;
      }
    }

    return implode($output, ' &#8594; '); // right arrow html entity
  }
}

/**
 * Implementation of hook_widget_info().
 */
function hier_cck_widget_info() {
  return array(
    'select' => array(
      'label' => 'Select List',
      'field types' => array('hier'),
    ),
    'radios' => array(
      'label' => 'Exposed hierarchy of checkboxes or radio buttons',
      'field types' => array('hier'),
    ),
    'check_select' => array(
      'label' => 'Hybrid of checkboxes/radios during node creation and select lists during updates',
      'field types' => array('hier'),
    ),
  );
}

/**
 * Implementation of hook_widget_settings().
 */
function hier_cck_widget_settings($op, $widget) {
  switch ($op) {
    case 'form':
      $form = array();
      $options = range(0, 10);
      $options[0] = t("Unlimited");
      $form['lvl'] = array(
        '#type'           => 'select',
        '#options'        => $options,
        '#title'           => t('Limit depth'),
        '#default_value'   => $widget['lvl'],
        '#description'     => t('Limit the hierarchy depth for items displayed on the editing form'),
      );
      return $form;

    case 'save':
      return array('lvl');
  }
}

/**
 * Implementation of hook_widget().
 */
function hier_cck_widget($op, &$node, &$field, &$items) {
  switch ($op) {
    case 'prepare form values':
      // Make sure that the hierarchy has been created.
      $field = hier_cck_field_init($field);
      return;

    case 'form':
      $tree = hier_cck_tree($field);

      // Special case: Hide this form if the node is the tree's root item.
      $root = $tree[0];
      if ($root->nid == $node->nid) {
        return;
      }

      $options = theme('hier_tree', $tree);

      // Always permit top-level items (TODO ???)
      if (!isset($options[$field['pid']])) {
        $options = array($field['pid'] => t('Root')) + $options;
      }

      // Add a null option.
      $options = array(0 => t('Please select')) + $options;

      // turn the widget into the select list for updating nodes
      if ($field['widget']['type'] == 'check_select' && $node->nid) {
        // not a new node, so use select lists
        $field['widget']['type'] = 'select';
      }

      // Build the form.
      $form = hier_cck_widget_form($field, $options, $items);

      if ($field['widget']['type'] == 'check_select') {
        return $form; // don't need AHAH hindings
      }

      // Add AHAH bindings.
      $path = 'hier_cck_widget/js/'. $field['field_name'] .'/'. $node->type;
      $form[$field['field_name']]['#ahah_bindings'] = array(
        array(
          'selector'    => '#'. $field['field_name'] .'-fieldset select',
          'event'        => 'change',
          'wrapper'      => $field['field_name'] .'-fieldset',
          'path'        => $path,
        ),
        array(
          'selector'    => '#'. $field['field_name'] .'-fieldset input',
          'event'        => 'change',
          'wrapper'      => $field['field_name'] .'-fieldset',
          'path'        => $path,
        )
      );

    return $form;

    case 'process form values':
      // Gather, sanitize and prepare item values from $_POST.
      // TODO the following is highly irresponsible.  hopefully fixing soon!
      $items = $_POST[$field['field_name']];
      $items = hier_cck_widget_form_values($field, $items);
      return;
  }
}

function hier_cck_widget_js($field_name, $content_type) {
  $field = content_fields($field_name, $content_type);
  $form_id = 'hier_cck_widget_form';
  $items = $_POST[$field_name];
  $form  = drupal_retrieve_form($form_id, $field, null, $items);
  drupal_prepare_form($form_id, $form);
  drupal_validate_form($form_id, $form);
  unset($form['form_token'], $form['form_id'], $form['form_build_id'], $form[$field_name]['#prefix'], $form[$field_name]['#suffix']);
  echo drupal_render($form[$field_name]);
}

function hier_cck_widget_form($field, $options = null, $items = null) {
  $form = array();

  // Set/get options from session, in case this was an AHAH call
  $options  = hier_cck_session($field, 'options', $options);

  // Gather, sanitize and prepare item values.
  if ($post = $_POST[$field['field_name']]) $items = $post;
  $items = hier_cck_widget_form_values($field, $items);

  // Don't allow an item to be its own parent
  foreach ($items as $i => $item) {
    unset($options[$item['hid']]);
  }

  if ($field['multiple']) {
    // Create a fieldset
    $form['#type']  = 'fieldset';
    $form['#title'] = t($field['widget']['label']);
  }

  $form['insert'] = array(
    '#type'           => $field['widget']['type'],
    '#theme'           => 'hier_cck_'. $field['widget']['type'],
    '#multiple'       => false,
    '#title'           => t($field['widget']['label']),
    '#default_value'   => $field['default'],
    '#options'         => $options,
    '#required'       => $field['required'],
    '#description'     => t($field['widget']['description']),
    '#parents'        => array($field['field_name'], 'insert'),
    '#id'              => 'edit-'. $field['field_name'] .'-insert',
    '#name'            => $field['field_name'] .'[insert]',
    '#prefix'         => '<div id="'. $field['field_name'] .'-insert">',
    '#suffix'         => '</div>',
  );

  if ($field['widget']['type'] == 'check_select') {
    // special cases for the check_select widget
    $form['insert']['#type'] = $field['multiple'] ? 'checkboxes' : 'radios';
    $form['insert']['#theme'] = 'hier_cck_radios';
    $form['insert']['#multiple'] = $field['multiple'];
    $form = array( $field['field_name'] => $form );
    // we don't need the update stuff, so short circuit
    return $form;
  }

  // Group the add/update fields together in a fieldset
  if ($field['multiple']) {
    $form['insert']['#title'] = t('Add a placement for !name', array('!name' => $field['widget']['label']));
  }

  $form['update'] = array();
  $form['update']['#prefix'] = '<div id="'. $field['field_name'] .'-update">';
  $form['update']['#suffix'] = '</div>';
  $form['update'][] = array(
    '#type'          => 'markup',
    '#value'        => t('Update placement'),
  );

  foreach ($items as $i => $item) {
    if (!$item['hid'] && !$item['parent']) continue;
    if (!$item['hid']) $item['hid'] = 'a'. $i;

    if ($field['multiple']) {
        // Prepare a formatted title that shows full lineage.
        $titles = array();
        foreach ($options as $k => $t) {
          $depth = strspn($t, '--');
          $titles[$depth] = preg_replace('/^-*(.*)/', '\1', $t);
          if ($k == $item['parent']) break;
        }
        foreach ($titles as $k => $t) {
          if ($k == 0 || $k > $depth) unset($titles[$k]);
        }

        ksort($titles);
        $title = join(' > ', $titles);
         $form['update'][$item['hid']] = array(
           '#type'           => 'select',
           '#multiple'       => false,
           '#title'           => $title,
           '#options'         => $options,
           '#default_value'  => array($item['parent']),
           '#value'  => array($item['parent']),
           '#required'       => false,
           '#parents'        => array($field['field_name'], 'update', $item['hid'] ),
          '#id'              => 'edit-'. $field['field_name'] .'-update-'. $item['hid'],
          '#name'            => $field['field_name'] .'[update]['. $item['hid'] .']',
         );
    }
    else {
      // Don't show a form to select new parents if this is an update on a
      // single-value field.
      $form['update'][$item['hid']] = $form['insert'];
      $form['update'][$item['hid']]['#default_value'] = $item['parent'];
      $form['insert'] = array('#type' => 'value', '#value' => array());
    }
  }

  $form['#prefix'] = '<div id="'. $field['field_name'] .'-fieldset">';
  $form['#suffix'] = '</div>';
  //$form['#tree'] = true;
  $form = array( $field['field_name'] => $form );

  return $form;
}

function hier_cck_widget_form_values($field, $input = null) {
  if (!$input) $input = array();

  $items = array();
  if (isset($input['insert'])) {
    if (!is_array($input['insert'])) {
      $input['insert'] = array($input['insert']);
    }
    foreach ($input['insert'] as $parent_hid) {
      if ($parent_hid) $items[] = array('parent' => $parent_hid);
    }
    unset($input['insert']);
  }

  if (isset($input['update'])) {
    if (!is_array($input['update'])) {
      $input['update'] = array($input['update']);
    }
    foreach ($input['update'] as $hid => $parent_hid) {
      if ($hid == false) continue;
      if (is_array($parent_hid)) $parent_hid = current($parent_hid);
      if (is_numeric($hid)) {
        $items[] = array('hid' => $hid, 'parent' => $parent_hid);
      }
      else {
        $items[] = array('parent' => $parent_hid);
      }
    }
    unset($input['update']);
  }
  $items = array_merge($items, $input);

  // Ensure that the 'parent' entry is populated for each hid
  foreach ($items as $i => $item) {
    if (isset($item['parent'])) continue;
    if ($item['hid'] == false) {
      unset($items[$i]);
    }
    else {
      $p = current(hier_parents($field['pid'], $item['hid'], null, 1));
      if (isset($p[$item['hid']]['parents'])) {
        $items[$i]['parent'] = current(array_keys($p[$item['hid']]['parents']));
      }
      else {
        unset($items[$i]);
      }
    }
  }

  return $items;
}

/*
 * Initialize a hier field: Called from hook_widget's 'prepare form values'
 *  to ensure that the field is related to a valid hierarchy.
 */
function hier_cck_field_init($field) {
  // Create a new hierarchy using hier's API.
  if (!isset($field['pid']) || !$field['pid']) {
    $field['pid'] = hier_create($field['field_name'], $field['widget']['label'])->pid;
    $update = true;
  }

  // Create a new view for item filters
  if (hier_cck_view_save($field)) {
    $update = true;
  }

  if ($update) {
    _content_admin_field_submit(null, $field);
  }
  return $field;
}

/*
 * Return a filtered list of items in the tree
 */
function hier_cck_tree($field) {
  if ($field['view'] && module_exists('views')) {
    $view = drupal_clone(views_load_view($field['view']));
    $data = views_build_view('items', $view, array(), false);
    $tree = $data['items'];
  }
  else {
    $tree = current(hier_tree($field['pid']));
  }
  if ($lvl = $field['widget']['lvl']) {
    foreach ($tree as $i => $h) {
      if ($h->lvl > $lvl) unset($tree[$i]);
    }
  }
  return $tree;
}

/*
 * Handler for views filters, returns a list of selectable items for this field
 */
function hier_cck_filter_options($op, $filterinfo) {
  $values = array();
  $pid = $filterinfo['pid'];
  if ($op == 'option') {
    $values = array(
      '***HIER_'. $pid .'_HID***'          => t('Current node'),
      '***HIER_'. $pid .'_POS***'       => t('Current position in hierarchy'),
    );
    if ($filterinfo && $pid) {
      $values = array_merge($values, theme('hier_tree', hier_cck_tree($filterinfo['content_field'])));
      $values[0] = t('Any');
    }
  }

  // $op = 'value' is actually handled via hook_form_alter()

  return $values;
}

/*
 * Implementation of hook_form_alter()
 * There is no way to access $filterdata or any other configurable setting
 * from within the filter options callback.  So, on exposed filters, we
 * form_alter our way to victory
 */
function hier_cck_form_alter($form_id, &$form) {
  if ($form_id != 'views_filters') return;
  $view = $form['view']['#value'];
  foreach ($view->exposed_filter as $filter) {
    if (substr($filter['field'], -11) == '_hid_parent') {
      $filter_key = 'filter'. $filter['position'];
      foreach ($view->filter as $f) {
        if ($f['field'] == $filter['field']) {
          $values = $form[$filter_key]['#options'];
          $field = preg_replace('/(.*\.)?(.+)(_hid_parent|_hid_child)/', '\2', $filter['field']);
          $field = content_fields($field);
          $field['widget']['lvl'] = $f['option'];
          $values += theme('hier_tree', hier_cck_tree($field));
          unset($values[$field['pid']]);
          $form[$filter_key]['#options'] = $values;
        }
      }
    }
  }
}

function theme_hier_cck_select(&$element) {
  return theme('select', $element);
}

function theme_hier_cck_radios(&$element) {
//  var_dump($element);
  drupal_add_css(drupal_get_path('module', 'hier_cck') . '/hier.css');
  $type = $element['#multiple'] ? 'checkbox' : 'radio';

  $box = false; // a status indicator to remember if a box is open or not
  $boxes = 0;
  $depth = 0;
  $output = '';
  foreach(element_children($element) as $key) {
    $item =& $element[$key];
    if ($item['#return_value'] == 0) {
      unset($element[$key]);
      continue;
    }
    $item_depth = strspn($item['#title'], '-') / 2;

    // the following logic opens and closes divs for each 1st level item.
    if ($box && $item_depth <= 1) {
      $output .= '</div>';
      $box = false;
      ++$boxes;
      if ($boxes % 2 == 0) {
        $output .= '<br class = "clear" />';
      }
    }

    if (!$box && $item_depth) {
      $output .= '<div class = "hier-exposed-widget-group">';
      $box = true;
    }

    if ($item_depth == 1) {
      $output .= '<div class = "hier-exposed-root-item">';
    }

    $output .= theme($type, $item);

    if ($item_depth == 1) {
      $output .= '</div>';
    }
  }
  if ($box) $output .= '</div>';

  $output .= '<br class = "clear" />';

  return $output;
}
